/*
 * CProgressbar.cxx
 *
 *  Created on: 16.06.2016
 *      Author: tkallage
 */

#include "CProgressbar.h"
#include "CStatusbar.h"

using namespace std;

namespace tkal {

CProgressbar::CProgressbar(CStatusbar &pStatusbar, int pLine) :
		fStatusbar(pStatusbar), fLine(pLine), fProgress(0.), fFillChar('#') {
	fStatusbar.writeLine("Progressbar initialized on line " + to_string(fLine), fLine);
}

CProgressbar::~CProgressbar() {
	fStatusbar.deleteLine(fLine);
}

void CProgressbar::update() {
	string str = "";
	int statusbarWidth = fStatusbar.getConsoleWidth();
	for (int i(0); i < statusbarWidth; i++) {
		if ((i + 1.) / statusbarWidth < fProgress) {
			str += fFillChar;
		} else {
			str += " ";
		}
	}
	fStatusbar.writeLine(str, fLine, true);
}

void CProgressbar::setFillChar(char pNewFillChar) {
	fFillChar = pNewFillChar;
	update();
}

void CProgressbar::setProgress(double pProgress, bool doUpdate) {
	fProgress = pProgress;
	if (doUpdate)
		update();
}

} /* namespace tkallage */
