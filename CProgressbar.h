/*
 * CProgressbar.h
 *
 *  Created on: 16.06.2016
 *      Author: tkallage
 */

#ifndef CPROGRESSBAR_H_
#define CPROGRESSBAR_H_

#include "CStatusbar.h"

namespace tkal {

class CProgressbar {
public:
	CProgressbar(CStatusbar &pStatusbar, int pLine = 1);
	virtual ~CProgressbar();
	int getLine() {
		return fLine;
	}
	double getProgress() {
		return fProgress;
	}
	void setProgress(double pProgress, bool doUpdate = true);
	char getFillChar() {
		return fFillChar;
	}
	void setFillChar(char pNewFillChar);
	void update();
private:
	CStatusbar fStatusbar;
	int fLine;
	double fProgress;
	char fFillChar;
};

} /* namespace tkallage */

#endif /* CPROGRESSBAR_H_ */
