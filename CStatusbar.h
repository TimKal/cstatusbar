/*
 * CStatusbar.h
 *
 *  Created on: 15.06.2016
 *      Author: tkallage
 */

#ifndef CSTATUSBAR_H_
#define CSTATUSBAR_H_

#include <string>

namespace tkal {

class CStatusbar {
public:
	CStatusbar(int pHeight = 3);
	virtual ~CStatusbar();
	void writeLine(std::string pText, int pLine, bool pFlush = true);
	void deleteLine(int pLine, bool pFlush = true);
	int getConsoleHeight();
	int getConsoleWidth();
	void debugStatusbar();
protected:
	int fHeight;
	bool fNProgressBars;
	void setScrollArea(int pY1, int pY2);
	void scrollLines(int pLines);
	void saveCursorPos();
	void unsaveCursorPos();
	void moveCursorX(int pLines = 1);
	void moveCursorY(int pLines = 1);
	void moveCursorTo(int pX, int pY);
	int getFirstStatusbarLine();
	std::string outOfBoundsError();
private:
	void createStatusbar();
	void removeStatusbar();
	std::string executeCommand(std::string pCommand);
};

} /* namespace tkallage */

#endif /* CSTATUSBAR_H_ */
