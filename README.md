# README #

## namespace ##
This package uses the namespace **tkal**.

## include ##

Easily include with
```
#!c++
#include "CProgressbar.h"

```
or, if you only want to use the Statusbar:
```
#!c++
#include "CStatusbar.h"

```

## usage ##
Example:
```
#!c++
#include "CProgressbar.h"

int main() {
  tkal::CStatusbar statusbar(3); // 3 is the height of the statusbar
  tkal::CProgressbar progressbar(statusbar,2); // add progressbar to 2. line of the statusbar
  statusbar.write("Statusbar begin",1,false); //write text in the 1. line of the statusbar, do not flush
  statusbar.write("Statusbar end",3); //write text in the 3. line of the statusbar and flush
  progressbar.setProgress(0.5); // Set Progress to 50%
  progressbar.setFillChar('*'); // Set fillchar to '*'
}

```

## known issues ##
*will be fixed in near future*

* progressbar can be overwritten by user
* write() cant handle '\n' properly
* not all functions that write to cout are flush-controllable
