/*
 * CStatusbar.cpp
 *
 *  Created on: 15.06.2016
 *      Author: tkallage
 */

#include "CStatusbar.h"

#include <stdio.h>
#include <string>
#include <iostream>
#include <unistd.h>

using namespace std;

namespace tkal {

CStatusbar::CStatusbar(int pHeight) :
		fHeight(pHeight), fNProgressBars(0) {
	createStatusbar();
	std::cout << "Created statusbar" << endl;
}

CStatusbar::~CStatusbar() {
	removeStatusbar();
}

void CStatusbar::createStatusbar() {
	scrollLines(fHeight);
	saveCursorPos();
	setScrollArea(1, getFirstStatusbarLine() - 1);
	unsaveCursorPos();
	moveCursorY(-fHeight);
	cout << flush;
}

void CStatusbar::removeStatusbar() {
	saveCursorPos();
	moveCursorTo(1, getFirstStatusbarLine() - 1);
	// delete status bar
	printf("\e[J");
	// reset scroll area
	printf("\e[r");
	unsaveCursorPos();
	cout << flush;
}

string CStatusbar::executeCommand(string pCommand) {
	string output;
	FILE* f = popen(pCommand.c_str(), "r");
	if (f == 0) {
		fprintf(stderr, "Could not execute: %s", pCommand.c_str());
		return "";
	}
	char buffer[1000];
	while (fgets(buffer, sizeof(buffer), f)) {
		output += buffer;
	}
	pclose(f);

	return output;
}

int CStatusbar::getConsoleHeight() {
	string heightString = executeCommand("tput lines");
	int height(atoi(heightString.c_str()));
	return height;
}

int CStatusbar::getConsoleWidth() {
	string widthString = executeCommand("tput cols");
	int width(atoi(widthString.c_str()));
	return width;
}

void CStatusbar::scrollLines(int pLines) {
	for (int i(0), N(abs(pLines)); i < N; i++) {
		// if lines > 0 scroll down, else scroll up
		if (pLines > 0)
			cout << "\eD";
		else
			cout << "\eM";
	}
}

void CStatusbar::setScrollArea(int pY1, int pY2) {
	if (pY1 > pY2)
		swap(pY1, pY2);
	printf("\e[%i;%ir", pY1, pY2);
}

void CStatusbar::saveCursorPos() {
	printf("\e[s");
}

void CStatusbar::unsaveCursorPos() {
	printf("\e[u");
}

void CStatusbar::moveCursorX(int pLines) {
	if (pLines < 0)
		printf("\e[%iD", abs(pLines));
	else
		printf("\e[%iC", abs(pLines));
}

void CStatusbar::moveCursorY(int pLines) {
	if (pLines < 0)
		printf("\e[%iA", abs(pLines));
	else
		printf("\e[%iB", abs(pLines));
}

void CStatusbar::moveCursorTo(int pX, int pY) {
	int h = getConsoleHeight();
	int w = getConsoleWidth();
	if (pX > w || pY > h) {
		cout << "CStatusbar::moveCursorTo(x=" << pX << ",y=" << pY << "):";
		cout << outOfBoundsError() << endl;
		return;
	}
	printf("\e[%i;%if", pY, pX);
}

int CStatusbar::getFirstStatusbarLine() {
	return getConsoleHeight() - fHeight + 1;
}

void CStatusbar::writeLine(string pText, int pLine, bool pFlush) {
	if (pLine > fHeight) {
		cout << "CStatusbar::writeLine(line=" << pLine << "):";
		cout << outOfBoundsError() << endl;
		return;
	}
	deleteLine(pLine, false);
	saveCursorPos();
	int x = 1;
	int y = getFirstStatusbarLine() + pLine - 1;
	moveCursorTo(x, y);
	cout << pText;
	unsaveCursorPos();
	if (pFlush)
		cout << flush;
}

void CStatusbar::deleteLine(int pLine, bool pFlush) {
	if (pLine > fHeight) {
		cout << "CStatusbar::deleteLine(pLine=" << pLine << "):";
		cout << outOfBoundsError() << endl;
		return;
	}
	saveCursorPos();
	int x = 1;
	int y = getFirstStatusbarLine() + pLine - 1;
	moveCursorTo(x, y);
	cout << "\e[2K";
	unsaveCursorPos();
	if (pFlush)
		cout << flush;
}

void CStatusbar::debugStatusbar() {
	for (int i(1); i <= fHeight; i++) {
		writeLine(to_string(i), i);
	}
}

string CStatusbar::outOfBoundsError() {
	int h = getConsoleHeight();
	int w = getConsoleWidth();
	string errorString = "";
	errorString += "[ERROR] Out of bounds.";
	errorString += "(h=" + to_string(h) + ",w=" + to_string(w) + ")";
	return errorString;
}

} /* namespace tkallage */
